package com.itau.jogoquiz.jogo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class SessaoJogo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private Long idJogo;
	private Long idJogador;
	private int quantidadeAcertos;
	private boolean ativo;
	@ManyToMany(cascade=CascadeType.ALL)
	private List<PerguntaSessao> idPergunta;
	
	
	public Long getId() {
		return id;
	}
	public Long getIdJogo() {
		return idJogo;
	}
	public void setIdJogo(Long idJogo) {
		this.idJogo = idJogo;
	}
	public Long getIdJogador() {
		return idJogador;
	}
	public void setIdJogador(Long idJogador) {
		this.idJogador = idJogador;
	}
	public int getQuantidadeAcertos() {
		return quantidadeAcertos;
	}
	public void setQuantidadeAcertos(int quantidadeAcertos) {
		this.quantidadeAcertos = quantidadeAcertos;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public List<PerguntaSessao> getIdPergunta() {
		return idPergunta;
	}
	public void setIdPergunta(List<PerguntaSessao> idPergunta) {
		this.idPergunta = idPergunta;
	}

	
	
	
	

}
