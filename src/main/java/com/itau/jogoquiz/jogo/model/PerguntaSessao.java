package com.itau.jogoquiz.jogo.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PerguntaSessao {
	
	@Id
	private long id;
	private boolean isRespondida;
	
	public PerguntaSessao() {}
	
	public PerguntaSessao(long id, boolean isRespondida) {
		this.id = id;
		this.isRespondida = isRespondida;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isRespondida() {
		return isRespondida;
	}

	public void setRespondida(boolean isRespondida) {
		this.isRespondida = isRespondida;
	}
	
	

}
