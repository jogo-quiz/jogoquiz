package com.itau.jogoquiz.jogo.model;

public class SessaoResponse {
	
	private Long idSessao;
	private String status;
	
	public Long getIdSessao() {
		return idSessao;
	}
	public void setIdSessao(Long idSessao) {
		this.idSessao = idSessao;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
