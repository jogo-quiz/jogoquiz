package com.itau.jogoquiz.jogo.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Jogo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String nome;
	private String descricao;
	private Integer quantidadeDePerguntas ;
	private Calendar dataCriacao;
	private Calendar dataUpdate;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getQuantidadeDePerguntas() {
		return quantidadeDePerguntas;
	}
	public void setQuantidadeDePerguntas(Integer quantidadeDePerguntas) {
		this.quantidadeDePerguntas = quantidadeDePerguntas;
	}
	
	public Calendar getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public Calendar getDataUpdate() {
		return dataUpdate;
	}
	public void setDataUpdate(Calendar dataUpdate) {
		this.dataUpdate = dataUpdate;
	}
	
	
	

}
