package com.itau.jogoquiz.jogo.model;

import java.util.List;

public class Pergunta {

	private long id;
	private String pergunta;
	private List<Resposta> respostas;
	
	
	public long getId() {
		return id;
	}
	public List<Resposta> getRespostas() {
		return respostas;
	}
	public void setRespostas(List<Resposta> respostas) {
		
		this.respostas = respostas;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPergunta() {
		return pergunta;
	}
	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("id "+id + "\n");
		builder.append("pergunta "+pergunta + "\n");
		
		for (Resposta resposta : respostas) {
			builder.append("resposta: "+resposta.getResposta() + "\n");
			builder.append("Certa "+resposta.isRespostaCorreta() + "\n");
			
		}
		
		
		return builder.toString();
		
		
	}
	

}
