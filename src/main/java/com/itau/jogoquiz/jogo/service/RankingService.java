package com.itau.jogoquiz.jogo.service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import javax.xml.ws.http.HTTPException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.itau.jogoquiz.jogo.model.JogadorResponse;
import com.itau.jogoquiz.jogo.model.PostRanking;
import com.itau.jogoquiz.jogo.model.SessaoJogo;

@Component
public class RankingService {
	
	@Autowired
	private ConnectionFactory connectionFactory;
	private static JmsTemplate jmsTemplate;

	private static RestTemplate rest =  new RestTemplate();
	 
	@PostConstruct
	  public void init() {
	      this.jmsTemplate = new JmsTemplate(connectionFactory);
	  }


	public static void enviaRankingHttp(Optional<SessaoJogo> sessaoJogo) {
		
		   String urlRanking = "http://10.162.109.18:8083/ranking";
		  
		   PostRanking ranking = new PostRanking();
		   ResponseEntity<PostRanking> resposta = null ;
		
			ranking.setIdJogador(sessaoJogo.get().getIdJogador().intValue());
			ranking.setIdJogo(sessaoJogo.get().getIdJogo().intValue());
			ranking.setPontos(new Long(sessaoJogo.get().getQuantidadeAcertos()));
			ranking.setDataUpdate(Calendar.getInstance());
			
			try {
					
				resposta = rest.postForEntity(urlRanking, ranking, PostRanking.class);
				
			} catch (Exception e) {
				throw new HTTPException(resposta.getStatusCodeValue());
			}
			
	}
		public static void enviaRankingJMS(SessaoJogo sessaoJogo) {
			   String urlJogador = "http://10.162.109.206:8082/jogador/"+sessaoJogo.getIdJogador();
			   
			  JogadorResponse jogador =  rest.getForObject(urlJogador, JogadorResponse.class);
			
			
				HashMap<String, String> body = new HashMap<>();
				body.put("player", jogador.getNome());
				body.put("game", sessaoJogo.getIdJogo().toString());
				body.put("hits", "1");
				body.put("total",String.valueOf(sessaoJogo.getQuantidadeAcertos()));
				
				jmsTemplate.convertAndSend("c1.queue.ranking", body);
		}
				
		
	}


