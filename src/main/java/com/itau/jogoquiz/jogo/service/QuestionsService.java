package com.itau.jogoquiz.jogo.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.itau.jogoquiz.jogo.model.Pergunta;
import com.itau.jogoquiz.jogo.model.PerguntasResponse;
import com.itau.jogoquiz.jogo.model.ProximaPerguntaResponse;
import com.itau.jogoquiz.jogo.model.SessaoJogo;
import com.itau.jogoquiz.jogo.model.SessaoResponse;

@Component
public class QuestionsService {
	@Autowired
	private ConnectionFactory connectionFactory;
	private static JmsTemplate jmsTemplate;
	private static final String baseUrl = "http://167.99.108.187:8084";

	private static RestTemplate rest =  new RestTemplate();
	 
	@PostConstruct
	  public void init() {
	      this.jmsTemplate = new JmsTemplate(connectionFactory);
	  }
	

	private static final Logger log = LoggerFactory.getLogger("QuestionService");
	
	
	public static List<Integer> getPerguntasHTTP(int qtde) {
		
		log.info("Buscando ids das perguntas, quantidade: " +qtde);
		
		String url = baseUrl+"/questions/"+qtde ;
		
		log.info(url);

		try {
			
			List<Integer> perguntas = rest.getForObject(url, List.class);
			return perguntas;
			
		} catch (Exception e) {
			log.error("Erro ao chamar servico de perguntas:   " +e.getMessage());
		}
		
		return null;
		

	}

	public static void pedirPerguntasJMS(Integer quantidadeDePerguntas) {
		HashMap<String, String> body = new HashMap<>();
		body.put("quantity", quantidadeDePerguntas.toString());
		jmsTemplate.convertAndSend("c1.queue.questions.qtd", body);

	}

	public static PerguntasResponse lerIdsPerguntasJMS() {
		
		
		return null;
	}
	
	public static Pergunta getDetalhesPergunta(String idPergunta) {
		
		log.info("Buscando detalhe da idPerguntas: " +idPergunta);
		
		try {
			
		
		String url = baseUrl+"/question/"+idPergunta;
		
		log.info("Buscando detalhe URL: " +url);
	    
		
		Pergunta pergunta = rest.getForObject(url, Pergunta.class);

		log.info("Retorno dos detalhes da Pergunta: " +pergunta.toString());
		
		return pergunta;
		
		} catch (Exception e) {
			log.error("Erro ao pegar os detalhes da pergunta: " +e.getMessage());
			
			return null;
		}
		
	}
}
