package com.itau.jogoquiz.jogo.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.jogoquiz.jogo.model.Jogo;

public interface JogoRepository extends CrudRepository<Jogo, Long> {

}
