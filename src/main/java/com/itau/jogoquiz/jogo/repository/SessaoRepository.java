package com.itau.jogoquiz.jogo.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.jogoquiz.jogo.model.SessaoJogo;

public interface SessaoRepository extends CrudRepository<SessaoJogo, Long>{

}
