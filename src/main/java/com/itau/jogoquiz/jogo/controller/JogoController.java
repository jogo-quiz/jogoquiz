package com.itau.jogoquiz.jogo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.jogoquiz.jogo.model.Jogo;
import com.itau.jogoquiz.jogo.model.Pergunta;
import com.itau.jogoquiz.jogo.model.PerguntaSessao;
import com.itau.jogoquiz.jogo.model.ProximaPerguntaResponse;
import com.itau.jogoquiz.jogo.model.SessaoJogo;
import com.itau.jogoquiz.jogo.model.SessaoResponse;
import com.itau.jogoquiz.jogo.repository.JogoRepository;
import com.itau.jogoquiz.jogo.repository.SessaoRepository;
import com.itau.jogoquiz.jogo.service.QuestionsService;
import com.itau.jogoquiz.jogo.service.RankingService;


@RestController
@RequestMapping("/jogo")
public class JogoController {
	
	@Autowired
	JogoRepository jogoRepository;
	
	@Autowired
	SessaoRepository sessaoRepository;

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	
	private RestTemplate rest =  new RestTemplate();
	
	@RequestMapping(path="/", method=RequestMethod.POST)
	public ResponseEntity<Jogo> incluirJogo(@RequestBody Jogo jogo){
		
		
		jogoRepository.save(jogo);
		
		
		return ResponseEntity.ok(jogo);
		
		
	}
		
	@RequestMapping(path="/iniciar/{idJogador}", method=RequestMethod.GET)
	public ResponseEntity<SessaoResponse> iniciarJogo(@PathVariable Long idJogador){
		log.info("## Classe: " + this.getClass().getName());
		log.info("Iniciando o jogo, idJogador: " + idJogador);
		Optional<Jogo> jogo = jogoRepository.findById(new Long(1));
		log.info("Encontrado jogo: " + jogo.get().getNome());
		
		SessaoResponse sessaoResponse  = new SessaoResponse();
		SessaoJogo sessao = new SessaoJogo();
		sessao.setAtivo(true);
		sessao.setIdJogador(idJogador);
		
		try {

		List<Integer> perguntas = QuestionsService.getPerguntasHTTP(jogo.get().getQuantidadeDePerguntas());
		log.info("Retornadas : " +perguntas.size()+" perguntas");
	 
	    List<Integer> idsPerguntas = new ArrayList<>();
	    
	    for(int i = 0 ; i < perguntas.size();i++) {
	    	idsPerguntas.add(perguntas.get(i));
	    	
	    }
	    sessao.setIdPergunta(new ArrayList<PerguntaSessao>());
	    for (Integer long1 : idsPerguntas) {
			sessao.getIdPergunta().add(new PerguntaSessao(long1, false));
		}
	    
	    log.info("Vai salvar a sessao" );
	    SessaoJogo sessaoSalva = sessaoRepository.save(sessao);
	    
	    log.info("Sessao Salva : " +sessaoSalva.getId());
	    sessaoResponse.setIdSessao(sessaoSalva.getId());
	    sessaoResponse.setStatus("SUCESSO");
	    
		
		return ResponseEntity.ok().body(sessaoResponse);
		} catch (Exception e) {
			
			sessaoResponse.setStatus("FALHA");
			log.error("Erro ao iniciar jogo: " +e.getMessage());
			return ResponseEntity.badRequest().build();
		}
		
		
	}
//	@RequestMapping(path="/iniciar/jms/{idJogador}", method=RequestMethod.GET)
//	public ResponseEntity<SessaoResponse> iniciarJogoJMS(@PathVariable Long idJogador){
//		
//		Optional<Jogo> jogo = jogoRepository.findById(new Long(1));
//		SessaoResponse sessaoResponse  = new SessaoResponse();
//		SessaoJogo sessao = new SessaoJogo();
//		sessao.setAtivo(true);
//		sessao.setIdJogador(idJogador);
//		
//		try {
//			
//			List<Integer> perguntas = QuestionsService.getPerguntasHTTP(jogo.get().getQuantidadeDePerguntas());
//	
//	 
//	    List<Integer> idsPerguntas = new ArrayList<>();
//	    
//	    for(int i = 0 ; i < perguntas.size();i++) {
//	    	idsPerguntas.add(perguntas.get(i));
//	    	
//	    }
//	    sessao.setIdPergunta(new ArrayList<PerguntaSessao>());
//	    for (Integer long1 : idsPerguntas) {
//			sessao.getIdPergunta().add(new PerguntaSessao(long1, false));
//		}
//	    SessaoJogo sessaoSalva = sessaoRepository.save(sessao);
//	    sessaoResponse.setIdSessao(sessaoSalva.getId());
//	    sessaoResponse.setStatus("SUCESSO");
//	    
//		
//		return ResponseEntity.ok().body(sessaoResponse);
//		} catch (Exception e) {
//			
//			sessaoResponse.setStatus("FALHA");
//			return ResponseEntity.badRequest().body(sessaoResponse);
//		}
//		
//		
//	}
//	
	
	@RequestMapping(path="/pergunta/{idSessao}", method=RequestMethod.GET)
	public ResponseEntity<ProximaPerguntaResponse> proximaPergunta(@PathVariable Long idSessao){
		
		log.info("## Classe: " + this.getClass().getName());
		log.info("Proxima pergunta, idSessao: " + idSessao);
		Optional<SessaoJogo> sessaoJogo =  sessaoRepository.findById(idSessao);
		ProximaPerguntaResponse proximaPerguntaResponse = new ProximaPerguntaResponse();
		
		try {
			log.info("Buscando detalhe da idPergunta: " +sessaoJogo.get().getIdPergunta().get(0).toString() );
			Pergunta pergunta  = QuestionsService.getDetalhesPergunta(String.valueOf(sessaoJogo.get().getIdPergunta().get(0).getId()));
			sessaoJogo.get().getIdPergunta().remove(0);
			log.info("Pergunta removida da sessao idPergunta: " +sessaoJogo.get().getIdPergunta().get(0).toString() );
			sessaoRepository.save(sessaoJogo.get());
		
		proximaPerguntaResponse.setId(String.valueOf(pergunta.getId()));
		proximaPerguntaResponse.setPergunta(pergunta.getPergunta());
		proximaPerguntaResponse.setOpcao1(pergunta.getRespostas().get(0).getResposta());
		proximaPerguntaResponse.setOpcao2(pergunta.getRespostas().get(1).getResposta());
		proximaPerguntaResponse.setOpcao3(pergunta.getRespostas().get(2).getResposta());
		proximaPerguntaResponse.setOpcao4(pergunta.getRespostas().get(3).getResposta());
		proximaPerguntaResponse.setAtivo(true);
		proximaPerguntaResponse.setPontosAcumulados(sessaoJogo.get().getQuantidadeAcertos());
		
		
		log.info("Respondendo detalhes da IdPergunta: " +pergunta.getId());
		return ResponseEntity.ok(proximaPerguntaResponse);
		
		
		} catch (Exception e) {
			
			try {
				
				log.info("Nao encontrou a proxima pergunta, mensagem: " +e.getMessage());
			
			sessaoJogo.get().setAtivo(false);
			proximaPerguntaResponse.setAtivo(false);
			proximaPerguntaResponse.setPontosAcumulados(sessaoJogo.get().getQuantidadeAcertos());

			RankingService.enviaRankingHttp(sessaoJogo);

			
			return ResponseEntity.ok().body(proximaPerguntaResponse);
			
			} catch (Exception e2) {
				
				log.error("Erro ao salvar o ranking" +e2.getMessage());
				return ResponseEntity.badRequest().build();
			}
			
			
		}
		
	
	}
	
	@RequestMapping(path="/teste/fila", method=RequestMethod.GET)
	public ResponseEntity<?> testeFila(){
		
		SessaoJogo sessao = new SessaoJogo();
		sessao.setAtivo(true);
		sessao.setIdJogador(new Long(1));
		sessao.setIdJogo(new Long(1));
		sessao.setQuantidadeAcertos(1000);
		
		
		try {
			RankingService.enviaRankingJMS(sessao);
			return ResponseEntity.ok().body("Deu bom");
			
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Deu Ruim " + e.getMessage());
		}
		
		
		
		
	}
  
	
	
	
	// TODO Fazer metodo de Resposta abaixo
	@RequestMapping(path="/resposta/{idSessao}/pergunta/{idPergunta}/opcao/{idOpcao}", method=RequestMethod.POST)
	public ResponseEntity<?> resposta(@PathVariable String idSessao,@PathVariable String idPergunta , @PathVariable String idOpcao ){
		
		log.info("Vai validar a resposta");
	
		try {
		SessaoJogo sessao = sessaoRepository.findById(Long.valueOf(idSessao)).get();
		Pergunta detalhesPergunta = QuestionsService.getDetalhesPergunta(idPergunta);
		
		log.info(detalhesPergunta.toString());
		log.info("Vai validar a resposta " + detalhesPergunta.getRespostas().get(Integer.parseInt(idOpcao)).isRespostaCorreta());
		
		
		
		if(detalhesPergunta.getRespostas().get(Integer.parseInt(idOpcao)).isRespostaCorreta()) {
			
			sessao.setQuantidadeAcertos(sessao.getQuantidadeAcertos()+1);
			sessaoRepository.save(sessao);

		}
		
		return ResponseEntity.ok("Resposta Salva");
	
	}catch(Exception e) {
		
	   log.error("Erro ao salvar resposta certa " +e.getMessage());
		return ResponseEntity.badRequest().body("Deu Ruim");
		
	}

	}
}
